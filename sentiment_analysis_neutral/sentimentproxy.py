from bert import run_classifier
import requests
import tensorflow as tf
from bert import run_classifier
from bert import optimization
import json
import argparse
import spacy
import pickle
import numpy as np

class sentimentproxy(object):

    def __init__(
            self,
            rest_endpoint=None,
            model_name=None):
       self.rest_endpoint = rest_endpoint+"/v1/models/"+model_name+":predict"
       self.model_name = model_name
        
    def predict(self, text, features_names=[]):

        # Loading pickled tokenizer
        with open('tokenizer.pickle', 'rb') as handle:
            tokenizer = pickle.load(handle)
        
        input_examples = [run_classifier.InputExample(guid="", text_a=text, text_b=None,label=0)]
        label_list=[0, 1, 2]
        label_dict=['Negative', 'Neutral', 'Positive']

        # see the script for the definition of the tokenizer
        input_features = run_classifier.convert_examples_to_features(input_examples, label_list, 50, tokenizer) 

        input_ids = np.array(input_features[0].input_ids) 
        label_ids = input_features[0].label_id
        segment_ids = np.array(input_features[0].segment_ids)
        input_masks = np.array(input_features[0].input_mask)


        # Right input format for tensorflow serving (see https://www.tensorflow.org/tfx/serving/api_rest#request_format_2 for more detail)
        tensor_dict = {"label_ids": [label_ids], "segment_ids": [segment_ids.tolist()],"input_mask":[input_masks.tolist()],"input_ids": [input_ids.tolist()]}
        data_dict = {"inputs": tensor_dict}
        data = json.dumps(data_dict)

        # Request to tensorflow serving predict API (requires the requests library)
        response = requests.post(self.rest_endpoint, data=data)
        response = response.json()

        max_label = response['outputs'][0].index(max(response['outputs'][0]))
        out_sentiment = label_dict[max_label]
        response['label']=max_label
        response['sentiment']=out_sentiment
        
        result = []
        result.append(response)

        #Entity Detection
        nlp_ent = spacy.load('./ner-model-spacy/')
        doc = nlp_ent(text)
        entities=[]
        for ent in doc.ents:
            entity = [ent.text, ent.label_]
            entities.append(entity)
        result.append(entities)
        return result

def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('-data', help='sentence', default='')
  args = parser.parse_args()
  data = args.data
  endpoint = "http://localhost:8080"
  modelname = "model_en"
  obj = sentimentproxy(endpoint, modelname)
  obj.predict(data,20)

if __name__== "__main__":
  main()
